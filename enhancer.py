import os
import json
import requests
import geocoder
import Geohash

from datetime import datetime
from influxdb import InfluxDBClient

import paho.mqtt.client as mqtt

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

print('influxdb host: %s' % os.environ.get('INFLUXDB_HOST', '0.0.0.0'))
print('influxdb db: %s' % os.environ.get('INFLUXDB_DB', 'restm'))
print('influxdb user: %s' % os.environ.get('INFLUXDB_USER', 'restm'))
print('influxdb pass: %s' % os.environ.get('INFLUXDB_USER_PASSWORD', 'restm'))

INFLUXDB_CLIENT = InfluxDBClient(
    host=os.environ.get('INFLUXDB_HOST', '0.0.0.0'),
    database=os.environ.get('INFLUXDB_DB', 'restm'),
    username=os.environ.get('INFLUXDB_USER', 'restm'),
    password=os.environ.get('INFLUXDB_USER_PASSWORD', 'restm')
)

GEO_API = os.environ.get('GEO_HOST', '0.0.0.0')

VALIDATE_KEYS = ['price', 'cover_surface']

# The callback for when the client receives a CONNACK response from the server.


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("#")

# The callback for when a PUBLISH message is received from the server.


def on_message(client, userdata, msg):
    print("Replicating mqtt event %s: %s" % (msg.payload, msg.topic))
    payload = json.loads(msg.payload)
    g = geocoder.google(payload['address'])
    if g.wkt:
        print("Geocoding succesful, proceeding")
        payload['wkt'] = g.wkt
        payload['geohash'] = Geohash.encode(g.lat, g.lng)
        polys = requests.get('http://%s:8000/api/?point=%s' % (GEO_API, g.wkt))
        print("Polys")
        if polys.status_code == 200:
            polys = polys.json()
            if 'neighborhood' in polys.keys() and 'grid' in polys.keys():
                payload['neighborhood'] = polys['neighborhood']['name']
                payload['grid'] = polys['grid']['uuid']
                keys = [k for k in payload.keys() if k not in VALIDATE_KEYS]
                tags = {}
                for k in keys:
                    tags[k] = payload[k]
                checks = list(set([k in payload.keys()
                                   for k in VALIDATE_KEYS]))
                print(checks)
                if False not in checks:
                    print("Sending data to influxdb")
                    if payload['cover_surface'] > 0:
                        INFLUXDB_CLIENT.write_points([
                            {
                                'tags': tags,
                                'fields': {
                                    'value': payload['price'],
                                    'surface': payload['cover_surface'],
                                    'value_per_unit_of_surface': payload['price'] / payload['cover_surface'],
                                },
                                'measurement': msg.topic,
                                'time': datetime.utcnow()
                            }
                        ])


MQTT_HOST = os.environ.get('MQTT_HOST', '0.0.0.0')
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(MQTT_HOST, 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
