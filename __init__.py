import os

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

from datetime import datetime

from influxdb import InfluxDBClient

DOCKER  = True if os.system("ping -c 1 influxdb > /dev/null 2>&1") is 0 else False

INFLUXDB_HOST = 'influxdb' if DOCKER else '0.0.0.0'

INFLUXDB_CLIENT = InfluxDBClient(
    host=INFLUXDB_HOST,
    database=os.environ.get('INFLUXDB_DB', 'reese'),
    username=os.environ.get('INFLUXDB_USER', 'reese'),
    password=os.environ.get('INFLUXDB_USER_PASSWORD', 'reese')
)

import paho.mqtt.client as mqtt

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print("Replicating mqtt event %s: %s" % (msg.payload, msg.topic))
    # INFLUXDB_CLIENT.write_points([
    #     {
    #         'tags': {},
    #         'fields': {
    #             'value': msg.payload,
    #         },
    #         'measurement': msg.topic,
    #         'time': datetime.utcnow()
    #     }
    # ])

MQTT_HOST = 'mqtt' if DOCKER else '0.0.0.0'
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(MQTT_HOST, 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()